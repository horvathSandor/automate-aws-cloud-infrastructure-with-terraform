# 0. Configuration of AWS Provider  
provider "aws" {
  region  = "us-east-1"
  access_key = "***********"   #keypair- allow us to connect to an instance, to the server
  secret_key = "***********"
}

variable "subnet_prefix" {                    # define a variable 
  description = "cidr block for the subnet"
}

# 1. Create VPC - virtual private cloud , private isolated network 
resource "aws_vpc" "prod-vpc" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "production"
  }
}

# 2. Create Internet Gateway - to send traffic out to the internet , wanna assign a public IP address to this server that anybody in the world can reach it
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prod-vpc.id

  tags = {
    Name = "prod"
  }
}

# 3. Create Custom Route Table - determines where network traffic from your subnet or gateway is directed. ipv4 and 6 is going to go towards the internet gateway so we can get out to the internet. 
resource "aws_route_table" "prod-route-table" {
  vpc_id = aws_vpc.prod-vpc.id

  route {
    cidr_block = "0.0.0.0/0"  # all traffic is going to be sent to the internet gateway
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "prod"
  }
}

# 4. Create a subnet
resource "aws_subnet" "prod-subnet" {
  vpc_id     = aws_vpc.prod-vpc.id # This is a reference to the ID of the above created VPC. This is how you reference other resources that you define in your code. provider_resourcetype.resourcename.id
  cidr_block = var.subnet_prefix[0].cidr_block  # reference a variable from terraform.tfvars
  availability_zone = "us-east-1a" 

  tags = {
    Name = var.subnet_prefix[0].name # reference variable
  }
}

resource "aws_subnet" "extra-subnet" {
  vpc_id     = aws_vpc.prod-vpc.id 
  cidr_block = var.subnet_prefix[1].cidr_block
  availability_zone = "us-east-1a" 

  tags = {
    Name = var.subnet_prefix[1].name 
  }
}

# 5. Associate subnet with route table
resource "aws_route_table_association" "prod-a" {
  subnet_id      = aws_subnet.prod-subnet.id
  route_table_id = aws_route_table.prod-route-table.id
}

# 6. Create security group to allow port 22,80,443 - responsible for determining what kind of traffic is allowed to get to any one of your EC2 instances. 
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow web inbound traffic"
  vpc_id      = aws_vpc.prod-vpc.id

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]  #Any IP address can access it
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
    ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0  # allowing all ports 
    to_port          = 0
    protocol         = "-1" # -1 means any protocol 
    cidr_blocks      = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_web"
  }
}

# 7. Create a network interface with an ip in the subnet that was created in step 4.
resource "aws_network_interface" "web-server-nic" {
  subnet_id       = aws_subnet.prod-subnet.id
  private_ips     = ["10.0.1.50"]  # private ip address for the host
  security_groups = [aws_security_group.allow_web.id]

}

# 8. Assign an Elastic IP to the network interface created in step 7. -  public IP address , anybody on the internet can access it. 
resource "aws_eip" "one" {
  domain                    = "vpc"
  network_interface         = aws_network_interface.web-server-nic.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [aws_internet_gateway.gw]   # explicit dependency on the IGW, EIP requires IGW to exit prios to association. 
}

# 9. Create ubuntu server and install/enable apache2
resource "aws_instance" "web-server-instance" {   # "<provider>_<resource type>" "<resource name>" 
    ami = "ami-0fc5d935ebf8bc3bc"
    instance_type = "t2.micro"
    availability_zone = "us-east-1a"
    key_name = "keypair"

    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.web-server-nic.id
    }

    tags = {
        Name = "ubuntu"
    }

  user_data = <<EOF
  #!/bin/bash
  sudo apt update -y
  sudo apt install apache2 -y
  sudo systemctl start apache2
  EOF  

}

  output "server_public_ip" {
    value       = aws_eip.one.public_ip
  }